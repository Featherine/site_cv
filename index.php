<?php 
 
include 'config/library.php';

if(isset($_GET["page"])==FALSE || $_GET["page"]==""){
	include 'controllers/home.php';
}

if (isset($_GET["page"])){
  if ($_GET["page"]=="home-mobile"){
		include 'controllers/homeMobile.php';
	}

	else if ($_GET["page"]=="curriculum-vitae"){
		include 'controllers/cv.php';
	}

	else if ($_GET["page"]=="exemples"){
		include 'controllers/exemples.php';
	}

	else if ($_GET["page"]=="galerie"){
		include 'controllers/galerie.php';
	}

	else if ($_GET["page"]=="card-game"){
		include 'controllers/cartes.php';
	}

	else if ($_GET["page"]=="a-propos"){
		include 'controllers/about.php';
	}

	else if ($_GET["page"]=="contact"){
		include 'controllers/contact.php';
	}

	else{
		header('Location: index.php');
    exit();
	}
}

include $layout;