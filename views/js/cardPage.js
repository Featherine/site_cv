"use strit";

var deck=[
			"As_of_diamonds.png",
			"King_of_diamonds_fr.png",
			"Queen_of_diamonds_fr.png",
			"Jack_of_diamonds_fr.png",
			"As_of_clubs_A.png",
			"King_of_clubs_fr.png",
			"Queen_of_clubs_fr.png",
			"Jack_of_clubs.png",
			"As_of_spades_A.png",
			"King_of_spades.png",
			"Queen_of_spades_fr.png",
			"Jack_of_spades_fr.png",
			"As_of_hearts_A.png",
			"King_of_hearts_fr.png",
			"Queen_of_hearts.png",
			"Jack_of_hearts_fr.png"
			];

var cardName=[
				"l'As de Carreau",
				"le Roi de Carreau",
				"la Dame de Carreau",
				"le Valet de Carreau",
				"l'As de Trèfle",
				"le Roi de Trèfle",
				"la Dame de Trèfle",
				"le Valet de Trèfle",
				"l'As de Pique",
				"le Roi de Pique",
				"la Dame de Pique",
				"le Valet de Pique",
				"l'As de Coeur",
				"le Roi de Coeur",
				"la Dame de Coeur",
				"le Valet de Coeur"
			];

var bentoDeck=[
				"Queen_of_hearts.png",
				"King_of_diamonds_fr.png",
				"Jack_of_clubs.png",
				"As_of_spades_A.png"			
				]

var dosCarte="views/pictures/cardDeck/Dos-de-cartes.jpg";


var card;
var oldCard;
var BentoCard=[];
var Queen;
var King;
var Jack;
var As;

function getRandom(min, max){
	oldCard = card
	card= Math.floor(Math.random() * (min,max));

	if(oldCard==card){
		card= Math.floor(Math.random() * (min,max));
	}
	
	else{
		return card;
	}
}

function RandomCard(event){
	event.preventDefault();
	getRandom(0,16);
	console.log(card);
	$("#NomDeCarte").text("Votre Carte est "+ cardName[card]);
	$("#VotreCarte").attr("src", 'views/pictures/cardDeck/'+deck[card]);

}

/* pour avoir ne pas avoir deux fois la même carte*/
function AttributeCard(){
	while(BentoCard.length < 4){
    	var i = Math.floor(Math.random() * (0,4));
    	if(BentoCard.indexOf(i) === -1){ 
			BentoCard.push(i);
		}
	}
	console.log(BentoCard);
	Queen= BentoCard[0];
	King= BentoCard[1]
	Jack= BentoCard[2]
	As= BentoCard[3]
	console.log(Queen, King, Jack, As);
}

function ShuffleBentoDeck(event){
	event.preventDefault();
	$('.CetteCarteDeux').removeData('id');
	BentoCard=[];
	AttributeCard();

	$(".CetteCarteDeux").css("transform", "");
	$('.CetteCarteDeux').attr('src',dosCarte);
	$("#blabla").text("Choisissez une carte !");
	$('#BentoUn').attr('data-id',bentoDeck[Queen]);
	$('#BentoDeux').attr('data-id',bentoDeck[King]);
	$('#BentoTrois').attr('data-id',bentoDeck[Jack]);
	$('#BentoQuatre').attr('data-id',bentoDeck[As]);
	$(".CetteCarteDeux").addClass("dos");
	$(".CetteCarteDeux").removeClass("face");
}

function RandomBento(event){
	event.preventDefault();

		$('#BentoUn').attr('src','views/pictures/cardDeck/'+bentoDeck[Queen]);
		$('#BentoDeux').attr('src','views/pictures/cardDeck/'+bentoDeck[King]);
		$('#BentoTrois').attr('src','views/pictures/cardDeck/'+bentoDeck[Jack]);
		$('#BentoQuatre').attr('src','views/pictures/cardDeck/'+bentoDeck[As]);

		$(".CetteCarteDeux").css("transform", "scale(1)");
		$(this).css("transform", "scale(1.1)");

		if($(this).data('id') == "Queen_of_hearts.png" && $(this).hasClass("dos")){
			$("#blabla").text("Bravo vous avez gagné !!!!");
		}

		else if($(this).data('id') == "notShuffleYet"){
			$("#blabla").text("Pas si vite! Il faut d'abord retourner les cartes voyons !");
			$('.CetteCarteDeux').attr('src',dosCarte);
		}

		else if($(this).data('id') != "Queen_of_hearts.png" && $(this).hasClass("dos")){
			$("#blabla").text("Dommage ce n'était pas la bonne carte !");
		}

		$(".CetteCarteDeux").removeClass("dos");
		$(".CetteCarteDeux").addClass("face");
}



$(function(){
	$('.CetteCarte').on('click', RandomCard);
	$('#BentoButton').on('click', ShuffleBentoDeck);
	$('.CetteCarteDeux').on('click', RandomBento);
})